from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.mail import send_mass_mail
from datetime import date

class Post(models.Model):
    place = models.CharField(max_length=100)
    description = models.TextField()
    photo = models.ImageField(upload_to='photos')
    author = models.CharField(max_length=30)
    creation_date = models.DateTimeField(auto_now_add=True)
    modification_date = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.place

    def save(self, *args, **kwargs):
        if self.pk:
            # set modification date only if modified
            self.modification_date = date.today()
        super(Post, self).save(*args, **kwargs)

class Newsletter(models.Model):
    email = models.EmailField(max_length=254, unique=True)

@receiver(post_save, sender=Post)
def send_newsletter(**kwargs):
    # sends emails to all newsletter subscribers after Post model is saved
    messages = []
    for subscriber in Newsletter.objects.all():
        message = (
            'Newsletter',
            'Wiadomość',
            'blog@example.com',
            [subscriber.email]
            )
        messages.append(message)

    send_mass_mail(
        messages,
        fail_silently=False
    )
