from rest_framework import serializers
from .models import Post, Newsletter

class PostSerializer(serializers.ModelSerializer):
    modification_date = serializers.DateField(read_only=True)

    class Meta:
        model = Post
        fields = '__all__'

class ContactSerializer(serializers.Serializer):
    email = serializers.EmailField()
    message = serializers.CharField()

class NewsletterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Newsletter
        fields = '__all__'
