from django.urls import path
from .views import (
    PostListAPIView, PostAPIView, ContactAPIView, 
    NewsletterListAPIView, NewsLetterAPIView
    )

urlpatterns = [
    path('', PostListAPIView.as_view()),
    path('<int:pk>/', PostAPIView.as_view()),
    path('kontakt/', ContactAPIView.as_view()),
    path('newsletter/', NewsletterListAPIView.as_view()),
    path('newsletter/<int:pk>/', NewsLetterAPIView.as_view())
    ]
