from django.core.mail import send_mail
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import (
    ListCreateAPIView, RetrieveUpdateDestroyAPIView, RetrieveDestroyAPIView
    )
from rest_framework.permissions import (
    BasePermission, IsAuthenticatedOrReadOnly, IsAuthenticated
    )
from rest_framework.filters import OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from .serializers import (
    PostSerializer, ContactSerializer, NewsletterSerializer
    )
from .models import Post, Newsletter

class IsAuthenticatedOrPostOnly(BasePermission):
    # allow permission if request method is POST or user is authenticated 
    def has_permission(self, request, view):
        return bool(
            request.method == 'POST' or
            request.user and
            request.user.is_authenticated
        )

class PostListAPIView(ListCreateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    ordering_fields = ['author']
    filterset_fields = ['creation_date', 'place']

class PostAPIView(RetrieveUpdateDestroyAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]

class ContactAPIView(APIView):
    def send_message(self, data):
        # sends email with question to blog owner
        message = data.get('message')
        from_email = data.get('email')
        subject = f'Wiadomość od {from_email}.'

        send_mail(
            subject,
            message,
            from_email,
            ['blog_owner@example.com'],
            fail_silently=False,
        )

    def post(self, request):
        serializer = ContactSerializer(data=request.data)

        if serializer.is_valid():
            self.send_message(serializer.validated_data)
            return Response({'success': 'Sent'}) 
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
                )

class NewsletterListAPIView(ListCreateAPIView):
    queryset = Newsletter.objects.all()
    serializer_class = NewsletterSerializer
    permission_classes = [IsAuthenticatedOrPostOnly]

class NewsLetterAPIView(RetrieveDestroyAPIView):
    queryset = Newsletter.objects.all()
    serializer_class = NewsletterSerializer
    permission_classes = [IsAuthenticated]
