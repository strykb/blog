## Instalacja i uruchomienie

```
git clone https://strykb@bitbucket.org/strykb/blog.git
cd blog
docker-compose build
docker-compose up
```
Dodanie danych inicjalizacyjnych:  
Docker -> Containers -> django-container -> CLI
```
python manage.py loaddata data.json
```
## Dane logowania
login: admin  
hasło: 123
## Adresy URL
Wpisy:  
[http://127.0.0.1:8000/](http://127.0.0.1:8000/)  
[http://127.0.0.1:8000/1](http://127.0.0.1:8000/1)  
Newsletter:  
[http://127.0.0.1:8000/newsletter/](http://127.0.0.1:8000/newsletter/)  
[http://127.0.0.1:8000/newsletter/1/](http://127.0.0.1:8000/1/)  
Kontakt:  
[http://127.0.0.1:8000/kontakt/](http://127.0.0.1:8000/kontakt/)
## Wysyłanie e-maili
E-maile są wypisywane w konsoli. Ustawienie to znajduje się na końcu pliku `settings.py`.  
Można je zmienić poprzez usunięcie
```python
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
```
i dodanie oraz uzupełnienie
```python
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_USE_TLS = True
```